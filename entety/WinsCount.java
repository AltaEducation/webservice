package entety;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Entity
@Table(name = "wins_goods")
public class WinsCount implements Serializable {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "count")
    private int count;


    @Column(name = "name")
    private String name;

    @Column(name = "winner")
    private String winner;


    @Column(name = "avg")
    private double avg;

    @Column(name = "winner_bin")
    private String bin;


    @Column(name = "description")
    private String desc;


    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @XmlElement
    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
    @XmlElement
    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }
    @XmlElement
    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }
    @XmlElement
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "WinsCount{" +
                "id=" + id +
                ", count=" + count +
                ", name='" + name + '\'' +
                ", winner='" + winner + '\'' +
                ", avg=" + avg +
                ", bin='" + bin + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
