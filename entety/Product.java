package entety;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Entity
@Table(name = "products")
public class Product implements Serializable {


    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "company")
    private String company;

    @Column(name = "tnved")
    private String tnved;

    @Column(name = "name")
    private String name;

    @Column(name = "producer")
    private String producer;

    @Column(name = "producer_address")
    private String address;

    @Column(name = "id_company",insertable = false,updatable = false)
    private int comID;



//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_company", referencedColumnName = "id", nullable = false)
//    private Company comp;
    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
    @XmlElement
    public String getTnved() {
        return tnved;
    }

    public void setTnved(String tnved) {
        this.tnved = tnved;
    }
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @XmlElement
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }
    @XmlElement
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @XmlElement
    public int getComID() {
        return comID;
    }

    public void setComID(int comID) {
        this.comID = comID;
    }

//    @XmlTransient
//    public Company getComp() {
//        return comp;
//    }
//
//    public void setComp(Company comp) {
//        this.comp = comp;
//    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", company='" + company + '\'' +
                ", tnved='" + tnved + '\'' +
                ", name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
