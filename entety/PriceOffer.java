package entety;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@XmlRootElement
@Entity
@Table(name = "offer")
public class PriceOffer implements Serializable {


    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @Column(name = "winner")
    private String winner;


    @Column(name = "winner_bin")
    private String winnerBIN;

    @Column(name = "winner_price")
    private double price;

    @Column(name = "lotNumber")
    private String lotNumber;

    @Column(name = "purNumber")
    private String purNumber;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lot_id")
    private Lot lot;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_id", referencedColumnName = "id", nullable = false)
    private Protocol protocol;
    @XmlElement
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
    @XmlElement
    public String getWinnerBIN() {
        return winnerBIN;
    }

    public void setWinnerBIN(String winnerBIN) {
        this.winnerBIN = winnerBIN;
    }
    @XmlElement
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    @XmlElement
    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    @XmlElement
    public String getPurNumber() {
        return purNumber;
    }

    public void setPurNumber(String purNumber) {
        this.purNumber = purNumber;
    }

    @XmlTransient
    public Lot getLot() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }

    @XmlTransient
    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    @Override
    public String toString() {
        return "PriceOffer{" +
                "id=" + id +
                ", winner='" + winner + '\'' +
                ", winnerBIN='" + winnerBIN + '\'' +
                ", price=" + price +
                ", lotNumber=" + lotNumber +
                ", purNumber=" + purNumber +
                '}';
    }
}
