
import dao.ProtocolDAO;
import dao.ProtocolDAOImp;
import entety.*;
import org.jboss.logging.Logger;
import util.ArrayListWrapper;
import util.ProtocolListWrapper;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Stateless
@Path("")
public class ProtocolResource {


    ProtocolDAO protocolDAO = new ProtocolDAOImp();

    private static Logger logger = Logger.getLogger(ProtocolResource.class);

    @GET
    @Path("ping")
    @Produces({MediaType.TEXT_HTML})
    public String ping() {
        logger.log(Logger.Level.INFO, "RESTful Protocol Service is running ==> ping");
        return "received ping on " + new Date().toString();
    }

    @GET
    @Path("protocols/lots/winner/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper winnerCountPrice(@DefaultValue("%") @QueryParam("winner") String winner,
                                             @DefaultValue("10") @QueryParam("count") int count,
                                             @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("winner", "%" + winner + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.priceArray = (ArrayList<Offer>) protocolDAO.totalPrice(query, page, count);
        return w;
    }

    @GET
    @Path("protocols/lots/winner/find")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper allWinners(@DefaultValue("%") @QueryParam("winner") String winner,
                                       @DefaultValue("10") @QueryParam("count") int count) {
        HashMap<String, String> query = new HashMap<>();
        query.put("winner", "%" + winner + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.myArray = (ArrayList<String>) protocolDAO.allWinners(query, count);
        return w;
    }

    @GET
    @Path("protocols/lots/find")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper allProduct(@DefaultValue("%") @QueryParam("name") String name,
                                       @DefaultValue("10") @QueryParam("count") int count) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.myArray = (ArrayList<String>) protocolDAO.getAllProducts(query, count);
        return w;
    }

    @GET
    @Path("protocols/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Protocol getProtocolByID(@PathParam("id") int id) {
        Protocol protocol = protocolDAO.getProtocolByID(id);
        return protocol;
    }


    @GET
    @Path("protocols/lots/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Lot getLotByID(@PathParam("id") int id) {
        Lot lot = protocolDAO.getLotByID(id);
        return lot;
    }

    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @GET
    @Path("protocols/")
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayList<Protocol> getAllProtocols(@DefaultValue("%") @QueryParam("number") String number,
                                               @DefaultValue("%") @QueryParam("name") String name,
                                               @DefaultValue("%") @QueryParam("organizer") String organizer,
                                               @DefaultValue("%") @QueryParam("venue") String venue,
                                               @DefaultValue("%") @QueryParam("purchase") String purchase,
                                               @DefaultValue("%") @QueryParam("source") String source,
                                               @DefaultValue("%") @QueryParam("region") String region,
                                               @DefaultValue("1") @QueryParam("lotCount") int lotCount,
                                               @DefaultValue("2010-03-28") @QueryParam("startDate") String startDate,
                                               @DefaultValue("2100-03-28") @QueryParam("endDate") String endDate,
                                               @DefaultValue("10") @QueryParam("count") int count,
                                               @DefaultValue("0") @QueryParam("page") int page) throws ParseException {
        HashMap<String, String> query = new HashMap<>();
        query.put("number", "%" + number + "%");
        query.put("name", "%" + name + "%");
        query.put("purchase", "%" + purchase + "%");
        query.put("source", "%" + source + "%");
        query.put("region", "%" + region + "%");
        query.put("organizer", "%" + organizer + "%");
        query.put("venue", "%" + venue + "%");
        Date end = sdf.parse(endDate);
        Date start = sdf.parse(startDate);
        List gos = protocolDAO.getPortocols(query, page, count, lotCount, end, start);
        ProtocolListWrapper w = new ProtocolListWrapper();
        w.ProtocolArray = (ArrayList<Protocol>) gos;
        logger.log(Logger.Level.INFO, "Return list of gos_protocols");
        return w.ProtocolArray;
    }


    @GET
    @Path("protocols/count/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper getProtocolCount(@DefaultValue("%") @QueryParam("number") String number,
                                             @DefaultValue("%") @QueryParam("name") String name,
                                             @DefaultValue("%") @QueryParam("organizer") String organizer,
                                             @DefaultValue("%") @QueryParam("venue") String venue,
                                             @DefaultValue("%") @QueryParam("purchase") String purchase,
                                             @DefaultValue("%") @QueryParam("source") String source,
                                             @DefaultValue("%") @QueryParam("region") String region,
                                             @DefaultValue("1") @QueryParam("lotCount") int lotCount,
                                             @DefaultValue("2010-03-28") @QueryParam("startDate") String startDate,
                                             @DefaultValue("2100-03-28") @QueryParam("endDate") String endDate) throws ParseException {
        HashMap<String, String> query = new HashMap<>();
        query.put("number", "%" + number + "%");
        query.put("name", "%" + name + "%");
        query.put("purchase", "%" + purchase + "%");
        query.put("source", "%" + source + "%");
        query.put("region", "%" + region + "%");
        query.put("organizer", "%" + organizer + "%");
        query.put("venue", "%" + venue + "%");
        Date end = sdf.parse(endDate);
        Date start = sdf.parse(startDate);
        ArrayListWrapper w = new ArrayListWrapper();
        w.countArray.add(protocolDAO.getProtocolCount(query, lotCount, start, end));
        return w;
    }


    @GET
    @Path("protocols/lots/count/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper getLotCount(@DefaultValue("%") @QueryParam("name") String name,
                                        @DefaultValue("%") @QueryParam("number") String number,
                                        @DefaultValue("%") @QueryParam("description") String desc,
                                        @DefaultValue("%") @QueryParam("protocol") String purNumber,
                                        @DefaultValue("1") @QueryParam("status") String status,
                                        @DefaultValue("10") @QueryParam("count") int count,
                                        @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("number", "%" + number + "%");
        query.put("description", "%" + desc + "%");
        query.put("purNumber", "%" + purNumber + "%");
        query.put("st", status);
        ArrayListWrapper w = new ArrayListWrapper();
        w.countArray.add(protocolDAO.getLotCount(query));
        return w;
    }


    @GET
    @Path("protocols/lots/")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public ProtocolListWrapper getLots(@DefaultValue("%") @QueryParam("name") String name,
                                       @DefaultValue("%") @QueryParam("number") String number,
                                       @DefaultValue("%") @QueryParam("description") String desc,
                                       @DefaultValue("%") @QueryParam("protocol") String purNumber,
                                       @DefaultValue("1") @QueryParam("status") String status,
                                       @DefaultValue("0") @QueryParam("unitPrice") String unitPrice,
                                       @DefaultValue("0") @QueryParam("unitCount") String unitCount,
                                       @DefaultValue("0") @QueryParam("price") String price,
                                       @DefaultValue("10") @QueryParam("count") int count,
                                       @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("number", "%" + number + "%");
        query.put("description", "%" + desc + "%");
        query.put("purNumber", "%" + purNumber + "%");
        query.put("st", status);
        List lots = protocolDAO.getLots(query, page, count);
        ProtocolListWrapper wrapper = new ProtocolListWrapper();
        wrapper.lotArray = (ArrayList<entety.Lot>) lots;
        return wrapper;
    }


    @GET
    @Path("protocols/lots/wincount")
    @Produces({MediaType.APPLICATION_JSON})
    public ProtocolListWrapper getOrderWin(@DefaultValue("%") @QueryParam("name") String name,
                                           @DefaultValue("%") @QueryParam("winner") String winner,
                                           @DefaultValue("10") @QueryParam("count") int count,
                                           @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("winner", "%" + winner + "%");
        List winCount = protocolDAO.getWinsCount(query, page, count);
        ProtocolListWrapper wrapper = new ProtocolListWrapper();
        wrapper.winsCounts = (ArrayList<WinsCount>) winCount;
        return wrapper;
    }

    @GET
    @Path("protocols/lots/wincount/count")
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayListWrapper getOrderWinCount(@DefaultValue("%") @QueryParam("name") String name,
                                             @DefaultValue("%") @QueryParam("winner") String winner,
                                             @DefaultValue("10") @QueryParam("count") int count,
                                             @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("winner", "%" + winner + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.countArray.add(protocolDAO.getOrderWinCount(query));
        return w;
    }

    @GET
    @Path("producer/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public ArrayList<Company> getCompanies(@DefaultValue("%") @QueryParam("name") String name,
                                           @DefaultValue("%") @QueryParam("branch") String branch,
                                           @DefaultValue("1") @QueryParam("region") String region,
                                           @DefaultValue("10") @QueryParam("count") int count,
                                           @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("branch", "%" + branch + "%");
        query.put("region", region);
        List producer = protocolDAO.getProducers(query, page, count);
        logger.log(Logger.Level.INFO, "Return list of producer");
        return (ArrayList<Company>) producer;
    }

    @GET
    @Path("producer/product/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayList<Product> getProduct(@DefaultValue("%") @QueryParam("name") String name,
                                         @DefaultValue("%") @QueryParam("company") String company,
                                         @DefaultValue("%") @QueryParam("tnved") String tnved,
                                         @DefaultValue("%") @QueryParam("producer") String producer,
                                         @DefaultValue("10") @QueryParam("count") int count,
                                         @DefaultValue("0") @QueryParam("page") int page) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("company", "%" + company + "%");
        query.put("tnved", "%" + tnved + "%");
        query.put("producer", "%" + producer + "%");
        List product = protocolDAO.getProducts(query, page, count);
        return (ArrayList<Product>) product;
    }

    @GET
    @Path("producer/product/count")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper getProductCount(@DefaultValue("%") @QueryParam("name") String name,
                                            @DefaultValue("%") @QueryParam("company") String company,
                                            @DefaultValue("%") @QueryParam("tnved") String tnved,
                                            @DefaultValue("%") @QueryParam("producer") String producer) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("company", "%" + company + "%");
        query.put("tnved", "%" + tnved + "%");
        query.put("producer", "%" + producer + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.countArray.add(protocolDAO.getProductCount(query));
        return w;
    }

    @GET
    @Path("producer/count/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper getCompaniesCount(@DefaultValue("%") @QueryParam("name") String name,
                                              @DefaultValue("%") @QueryParam("branch") String branch,
                                              @DefaultValue("1") @QueryParam("region") String region) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        query.put("branch", "%" + branch + "%");
        query.put("region", region);
        ArrayListWrapper w = new ArrayListWrapper();
        w.countArray.add(protocolDAO.getProducersCount(query));
        return w;
    }

    @GET
    @Path("producer/find")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper allProducer(@DefaultValue("%") @QueryParam("name") String name,
                                        @DefaultValue("10") @QueryParam("count") int count) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.myArray = (ArrayList<String>) protocolDAO.allProducer(query, count);
        return w;
    }

    @GET
    @Path("producer/product/find")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper allProdProduct(@DefaultValue("%") @QueryParam("name") String name,
                                           @DefaultValue("10") @QueryParam("count") int count) {
        HashMap<String, String> query = new HashMap<>();
        query.put("name", "%" + name + "%");
        ArrayListWrapper w = new ArrayListWrapper();
        w.myArray = (ArrayList<String>) protocolDAO.allProdProduct(query, count);
        return w;
    }

    @GET
    @Path("protocols/lots/avg")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    public ArrayListWrapper getAvg(@DefaultValue("%") @QueryParam("name") String name) {
        return protocolDAO.getAvg(name);
    }
}
