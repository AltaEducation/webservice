package util;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;
    private static final SessionFactory sessionFactory1;

    


    static {
        try {
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    static {
        try {
            sessionFactory1 = new Configuration().configure("hibernate1.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static void shutdownCSF() {
        // Close caches and connection pools
        getComSessionFactory().close();
    }

    public static SessionFactory getComSessionFactory() {
        return sessionFactory1;
    }


    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
