package util;

import entety.Lot;
import entety.Protocol;
import entety.WinsCount;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;


@XmlRootElement
public class ProtocolListWrapper {

    public ArrayList<Protocol> ProtocolArray = new ArrayList<>();
    public ArrayList<Lot> lotArray = new ArrayList<>();
    public ArrayList<WinsCount> winsCounts = new ArrayList<>();

}
