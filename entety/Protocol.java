package entety;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@XmlRootElement
@XmlSeeAlso(Lot.class)
@Entity
@Table(name = "protocol")
public class Protocol implements Serializable {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "number")
    private String number;

    @Column(name = "name")
    private String name;

    @Column(name = "organizer")
    private String organizer;

    @Column(name = "venue")
    private String venue;

    @Column(name = "method")
    private String purchaseMethod;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "lots_count")
    private int lotsCount;

    @Column(name = "source")
    private String source;

    @Column(name = "region")
    private String region;

    @OneToMany(mappedBy = "protocol",cascade = CascadeType.ALL,fetch=FetchType.EAGER)
    private List<Lot> lot;

    @OneToMany(mappedBy = "protocol",cascade = CascadeType.ALL)
    private List<PriceOffer> priceOffers;

    @XmlTransient
    public List<PriceOffer> getPriceOffers() {
        return priceOffers;
    }

    public void setPriceOffers(List<PriceOffer> priceOffers) {
        this.priceOffers = priceOffers;
    }

    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @XmlElement
    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }
    @XmlElement
    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }
    @XmlElement
    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }
    @XmlElement
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    @XmlElement
    public int getLotsCount() {
        return lotsCount;
    }

    public void setLotsCount(int lotsCount) {
        this.lotsCount = lotsCount;
    }
    @XmlElement
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    @XmlElement
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    @XmlElement
    public List<Lot> getLot() {
        return lot;
    }

    public void setLot(List<Lot> lot) {
        this.lot = lot;
    }

    @Override
    public String toString() {
        return "Protocol{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", organizer='" + organizer + '\'' +
                ", venue='" + venue + '\'' +
                ", purchaseMethod=" + purchaseMethod +
                ", endDate=" + endDate +
                ", lotsCount=" + lotsCount +
                ", source=" + source +
                ", region=" + region +
                ", lot=" + lot +
                '}';
    }
}
