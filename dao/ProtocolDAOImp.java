package dao;

import entety.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import util.ArrayListWrapper;
import util.HibernateUtil;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class ProtocolDAOImp implements ProtocolDAO {

    static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    static SessionFactory sessionFactory1 = HibernateUtil.getComSessionFactory();
    Session session = sessionFactory.openSession();
    Session session1 = sessionFactory1.openSession();

    @Override
    public Protocol getProtocolByID(int id) {
        session.beginTransaction();
        Protocol protocol = (Protocol) session.createQuery("from Protocol p where p.id = :id").setParameter("id", id)
                .getSingleResult();
        session.close();
        return protocol;
    }

    @Override
    public Lot getLotByID(int id) {
        session.beginTransaction();
        Lot protocol = (Lot) session.createQuery("from Lot l where l.id = :id").setParameter("id", id)
                .getSingleResult();
        session.close();
        return protocol;
    }

    @Override
    public List allWinners(Map<String, String> quer, int count) {
        session.beginTransaction();
        List winners = session.createQuery("SELECT l.winner as winner from PriceOffer l where l.winner like :winner group by l.winner")
                .setProperties(quer)
                .setMaxResults(count).getResultList();
        session.close();
        return winners;
    }


    @Override
    public List totalPrice(Map<String, String> quer, int page, int count) {
        session.beginTransaction();
        Criteria criteria = session.createCriteria(PriceOffer.class);
        criteria.add(Restrictions.like("winner", quer.get("winner")))
                .setProjection(Projections.projectionList()
                        .add(Projections.groupProperty("winner"), "name")
                        .add(Projections.sum("price"), "total"))
                .setMaxResults(count);
        List totalPrice = criteria.setResultTransformer(Transformers.aliasToBean(Offer.class)).list();
        session.close();
        return totalPrice;
    }

    @Override
    public List getAllProducts(Map<String, String> quer, int count) {
        session.beginTransaction();
        List products = session.createQuery("SELECT l.name as name from Lot l where l.name like :name group by l.name")
                .setProperties(quer)
                .setMaxResults(count).getResultList();
        session.close();
        return products;
    }

    @Override
    public Integer getProtocolCount(Map<String, String> quer, int lotCount, Date start, Date end) {
        session.beginTransaction();
        Integer count = ((Long) session.createQuery("select count(*) from Protocol p where p.number like :number " +
                "and p.source like :source " +
                "and p.name like :name " +
                "and p.region like :region " +
                "and p.purchaseMethod like :purchase  " +
                "and p.venue like :venue  " +
                "and p.lotsCount >= :lotsCount  " +
                "and p.endDate between :startDate and :endDate " +
                "and p.organizer like :organizer")
                .setParameter("lotsCount", lotCount)
                .setParameter("startDate", start)
                .setParameter("endDate", end)
                .setProperties(quer)
                .uniqueResult()).intValue();
        session.close();
        return count;
    }


    @Override
    public List getPortocols(Map<String, String> quer, int page, int count, int lotCount, Date end, Date start) {
        session.beginTransaction();
        List protocols;
        try {
            protocols = session.createQuery("from Protocol p where p.number like :number " +
                    "and p.source like :source " +
                    "and p.name like :name " +
                    "and p.region like :region " +
                    "and p.purchaseMethod like :purchase  " +
                    "and p.venue like :venue  " +
                    "and p.lotsCount >= :lotsCount  " +
                    "and p.endDate between :startDate and :endDate " +
                    "and p.organizer like :organizer ")
                    .setProperties(quer)
                    .setParameter("lotsCount", lotCount)
                    .setParameter("startDate", start)
                    .setParameter("endDate", end)
                    .setFirstResult((page - 1) * count)
                    .setMaxResults(count)
                    .getResultList();
            return protocols;
        } finally {
            session.close();
        }
    }


    @Override
    public Integer getLotCount(Map<String, String> quer) {
        session.beginTransaction();
        Integer count = ((Long) session.createQuery("select count(*) from Lot l where l.number like :number " +
                "and l.name like :name " +
                "and l.description like :description " +
                "and l.status = cast(:st as integer ) " +
                "and l.purNumber like :purNumber")
                .setProperties(quer)
                .uniqueResult()).intValue();
        session.close();
        return count;


    }


    @Override
    public List getLots(Map<String, String> quer, int page, int count) {
        session.beginTransaction();
        List lots;
        try {
            lots = session.createQuery("from Lot l where l.number like :number " +
                    "and l.name like :name " +
                    "and l.description like :description " +
                    "and l.status = cast(:st as integer )" +
                    "and l.purNumber like :purNumber")
                    .setProperties(quer)
                    .setFirstResult((page - 1) * count)
                    .setMaxResults(count)
                    .getResultList();
        } finally {
            session.close();
        }
        return lots;
    }

    @Override
    public List getWinsCount(Map<String, String> quer, int page, int count) {
        session.beginTransaction();
        List winCount;
        try {
            winCount = session.createQuery("from WinsCount w where w.name like :name " +
                    "and w.winner like :winner")
                    .setProperties(quer)
                    .setFirstResult((page - 1) * count)
                    .setMaxResults(count)
                    .getResultList();
        } finally {
            session.close();
        }
        return winCount;
    }

    @Override
    public Integer getOrderWinCount(Map<String, String> quer) {
        session.beginTransaction();
        Integer count;
        try {
            count = ((Long) session.createQuery("select count(*) from WinsCount w where w.name like :name " +
                    "and w.winner like :winner ")
                    .setProperties(quer)
                    .uniqueResult()).intValue();
            return count;
        } finally {
            session.close();
        }
    }

    @Override
    public List getProducers(Map<String, String> quer, int page, int count) {
        session1.beginTransaction();
        List company;
        try {
            company = session1.createQuery("from Company c where c.name like :name " +
                    "and c.regionID = cast(:region as integer ) " +
                    "and c.branch like :branch")
                    .setProperties(quer)
                    .setFirstResult((page - 1) * count)
                    .setMaxResults(count)
                    .getResultList();
            return company;
        } finally {
            session1.close();
        }

    }

    @Override
    public List getProducts(Map<String, String> quer, int page, int count) {
        session1.beginTransaction();
        List products;
        try {
            products = session1.createQuery("from Product p where p.name like :name " +
                    "and p.company like :company " +
                    "and p.tnved like :tnved " +
                    "and p.producer like :producer")
                    .setProperties(quer)
                    .setFirstResult((page - 1) * count)
                    .setMaxResults(count)
                    .getResultList();
            return products;
        } finally {
            session1.close();
        }
    }

    @Override
    public Integer getProducersCount(Map<String, String> quer) {
        session1.beginTransaction();
        Integer count = ((Long) session1.createQuery("select count(*) from Company c where c.name like :name " +
                "and c.regionID = cast(:region as integer ) " +
                "and c.branch like :branch")
                .setProperties(quer)
                .uniqueResult()).intValue();
        session1.close();
        return count;
    }

    @Override
    public Integer getProductCount(Map<String, String> quer) {
        session1.beginTransaction();
        Integer count = ((Long) session1.createQuery("select count(*) from Product p where p.name like :name " +
                "and p.company like :company " +
                "and p.tnved like :tnved " +
                "and p.producer like :producer")
                .setProperties(quer)
                .uniqueResult()).intValue();
        session1.close();
        return count;
    }

    @Override
    public List allProducer(Map<String, String> query, int count) {
        session1.beginTransaction();
        List companies = session1.createQuery("SELECT p.name as name from Company p where p.name like :name  group by p.name")
                .setProperties(query)
                .setMaxResults(count).getResultList();
        session1.close();
        return companies;
    }

    @Override
    public List allProdProduct(Map<String, String> query, int count) {
        session1.beginTransaction();
        List products = session1.createQuery("SELECT p.name as name from Product p where p.name like :name  group by p.name")
                .setProperties(query)
                .setMaxResults(count).getResultList();
        session1.close();
        return products;
    }

    @Override
    public ArrayListWrapper getAvg(String name) {
        ArrayListWrapper wrapper = new ArrayListWrapper();
        session.beginTransaction();
        Query query = session.createNativeQuery("SELECT count(name),avg(unit_price) from lots as l" +
                " WHERE l.name LIKE :name");
        query.setParameter("name", "%"+name+"%");
        List<Object[]> results = query.getResultList();
        results.stream().forEach((result) -> {
            Jedi jedi = new Jedi();
            jedi.setCountName((BigInteger) result[0]);
            jedi.setAvgPrice((double) result[1]);
            wrapper.jediArray.add(jedi);
        });
        session.close();
        return wrapper;

    }


}
