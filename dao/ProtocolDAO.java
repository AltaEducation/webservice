package dao;

import entety.Lot;
import entety.Protocol;
import util.ArrayListWrapper;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface ProtocolDAO {
    List getPortocols(Map<String,String > query, int page, int count, int lotCount, Date end,Date start);

    List getLots(Map<String,String > query, int page, int count);

    List totalPrice(Map<String,String > query, int page, int count);

    List getAllProducts(Map<String,String > query, int count);

    List allWinners(Map<String,String > query, int count);

    List allProducer(Map<String,String > query, int count);

    List allProdProduct(Map<String,String > query, int count);

    Protocol getProtocolByID(int id);

    Lot getLotByID(int id);

    Integer getProtocolCount(Map<String, String> quer,int lotCount,Date start,Date end);

    Integer getLotCount(Map<String, String> quer);

    List getWinsCount(Map<String, String> quer, int page, int count);

    Integer getOrderWinCount(Map<String, String> quer);

    Integer getProducersCount(Map<String, String> quer);

    Integer getProductCount(Map<String, String> quer);

    List getProducers(Map<String, String> quer, int page, int count);

    List getProducts(Map<String, String> quer, int page, int count);

    ArrayListWrapper getAvg(String name);



}
