package entety;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Entity
@Table(name = "companies")
public class Company implements Serializable {


    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "bin")
    private String bin;

    @Column(name = "region")
    private String region;


    @Column(name = "regionID")
    private int regionID;

    @Column(name = "oked")
    private int oked;

    @Column(name = "branch")
    private String branch;

//    @OneToMany(mappedBy = "comp",cascade = CascadeType.ALL,fetch=FetchType.EAGER)
//    private List<Product> product;

    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @XmlElement
    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }


    @XmlElement
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }


    @XmlElement
    public int getOked() {
        return oked;
    }

    public void setOked(int oked) {
        this.oked = oked;
    }


    @XmlElement
    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
//    @XmlElement
//    public List<Product> getProduct() {
//        return product;
//    }
    @XmlElement
    public int getRegionID() {
        return regionID;
    }

    public void setRegionID(int regionID) {
        this.regionID = regionID;
    }

//    public void setProduct(List<Product> product) {
//        this.product = product;
//    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bin='" + bin + '\'' +
                ", region='" + region + '\'' +
                ", regionID=" + regionID +
                ", oked=" + oked +
                ", branch='" + branch + '\'' +
//                ", product=" + product +
                '}';
    }
}
