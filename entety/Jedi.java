package entety;


import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigInteger;


@Entity
public class Jedi implements Serializable {




    private BigInteger countName;
    private double avgPrice;


    @Override
    public String toString() {
        return "Jedi{" +
                "countName=" + countName +
                ", avgPrice=" + avgPrice +
                '}';
    }

    public BigInteger getCountName() {
        return countName;
    }

    public void setCountName(BigInteger countName) {
        this.countName = countName;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(double avgPrice) {
        this.avgPrice = avgPrice;
    }
}
