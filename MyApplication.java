import dao.ProtocolDAOImp;
import util.HibernateUtil;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.*;


@ApplicationPath("/")
public class MyApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        System.out.println("REST configuration starting: getClasses()");
        resources.add(org.glassfish.jersey.moxy.json.MoxyJsonFeature.class);
        resources.add(JsonMoxyConfigurationContextResolver.class);
        resources.add(ProtocolResource.class);
        resources.add(HibernateUtil.class);
        resources.add(ProtocolDAOImp.class);
        resources.add(HeaderFilter.class);
        System.out.println("REST configuration ended successfully.");
        return resources;
    }

    @Override
    public Set<Object> getSingletons() {
        return Collections.emptySet();
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("jersey.config.server.wadl.disableWadl", true);

        return properties;
    }
}
