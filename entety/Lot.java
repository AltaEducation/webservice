package entety;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@XmlRootElement
@XmlSeeAlso(PriceOffer.class)
@Entity
@Table(name = "lots")

public class Lot implements Serializable {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "number")
    private String number;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "count")
    private double count;

    @Column(name = "unit_price")
    private double unitPrice;

    @Column(name = "unit")
    private String unit;

    @Column(name = "stausNumber")
    private int status;

    @Column(name = "price")
    private Double price;

    @Column(name = "purNumber")
    private String purNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_id", referencedColumnName = "id", nullable = false)
    private Protocol protocol;

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "lot", cascade = CascadeType.ALL)
    private PriceOffer offer;
    @XmlElement
    public PriceOffer getOffer() {
        return offer;
    }

    public void setOffer(PriceOffer offer) {
        this.offer = offer;
    }
    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @XmlElement
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @XmlElement
    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }
    @XmlElement
    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    @XmlElement
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    @XmlElement
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @XmlTransient
    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }
    @XmlElement
    public Double getPrice() {
        return price;
    }
    @XmlElement
    public String getPurNumber() {
        return purNumber;
    }

    public void setPurNumber(String purNumber) {
        this.purNumber = purNumber;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", unitPrice=" + unitPrice +
                ", unit='" + unit + '\'' +
                ", status='" + status + '\'' +
                ", price='" + price + '\'' +
                ", purNumber='" + purNumber + '\'' +
                ", offer='" + offer + '\'' +
                '}';
    }
}
