package util;

import entety.Jedi;
import entety.Offer;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class ArrayListWrapper {
    public ArrayList<String> myArray = new ArrayList<>();

    public ArrayList<Integer> countArray = new ArrayList<>();

    public ArrayList<Offer> priceArray = new ArrayList<>();

    public ArrayList<Jedi> jediArray = new ArrayList<>();

}